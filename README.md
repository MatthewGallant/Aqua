![Aqua Logo](icon.png)

# Aqua
Aqua is a frontend for Hydrogen, a basic html engine written in python.

---

### About Aqua
Aqua is a front end for Hydrogen, the basic html engine bundled with Aqua. Aqua is a small project for a basic html interpreter in python.

---

### License
Aqua is available under the MIT License, see [LICENSE](https://github.com/ChilliNerd/Aqua/blob/master/LICENSE).

---

### HTML Support
Hydrogen supports h1-h6, p and button tags.

---

### Running
For macOS users, there is a .app file available in the Executables/ folder.

Otherwise, you must run the python script, which is Aqua.py.

### Credits
Developed by Matt Gallant(ChilliNerd).

Icon by Arun Thomas(https://www.iconfinder.com/arunxthomas)

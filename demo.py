#
# Hydrogen WebView API Demo
#
# (c) 2016 Matt Gallant
#

import hydrogen
from Tkinter import *

root = Tk()

hydrogen.hydrogen_all()

root.geometry("500x500+150+150")
root.title(hydrogen.title)
root.mainloop()
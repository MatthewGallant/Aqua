#
# Hydrogen Web Engine
# Version 1.0
#
# Licensed under the MIT license
#
# Created by matthewgallant on 11/7/16
#
# (c) 2016 Matthew Gallant
#

# Imports
from Tkinter import *
from sys import argv
from sys import platform

# Handle File Arguments
txt = open('/Users/matthewgallant/Desktop/Aqua/demo.html', 'r')
text = txt.read()

# Variables
title_var = ""
s = " "

# Tags
for line in text.splitlines():
    if "<h1>" in line:
        h1_var_split = s.join(line.split())
        h1_var = h1_var_split[4:-5]
    if "<h2>" in line:
        h2_var_split = s.join(line.split())
        h2_var = h2_var_split[4:-5]
    if "<h3>" in line:
        h3_var_split = s.join(line.split())
        h3_var = h3_var_split[4:-5]
    if "<h4>" in line:
        h4_var_split = s.join(line.split())
        h4_var = h4_var_split[4:-5]
    if "<h5>" in line:
        h5_var_split = s.join(line.split())
        h5_var = h5_var_split[4:-5]
    if "<h6>" in line:
        h6_var_split = s.join(line.split())
        h6_var = h6_var_split[4:-5]
    if "<p>" in line:
        p_var_split = s.join(line.split())
        p_var = p_var_split[3:-4]
    if "<button>" in line:
        button_var_split = s.join(line.split())
        button_var = button_var_split[8:-9]
    if "<title>" in line:
        title_var_split = s.join(line.split())
        title = title_var_split[7:-8]

# Import All
def hydrogen_all():
    h1_label = Label(text=h1_var, font=('Helvetica', 32))
    h1_label.grid(sticky=NW)
    h2_label = Label(text=h2_var, font=('Helvetica', 28))
    h2_label.grid(sticky=NW)
    h3_label = Label(text=h3_var, font=('Helvetica', 19))
    h3_label.grid(sticky=NW)
    h4_label = Label(text=h4_var, font=('Helvetica', 15))
    h4_label.grid(sticky=NW)
    h5_label = Label(text=h5_var, font=('Helvetica', 13))
    h5_label.grid(sticky=NW)
    h6_label = Label(text=h6_var, font=('Helvetica', 11))
    h6_label.grid(sticky=NW)
    p_label = Label(text=p_var, font=('Helvetica', 11))
    p_label.grid(sticky=NW)
    button_button = Button(text=button_var)
    button_button.grid(sticky=NW)

def hydrogen_all_center():
    h1_label = Label(text=h1_var, font=('Helvetica', 32))
    h1_label.pack()
    h2_label = Label(text=h2_var, font=('Helvetica', 28))
    h2_label.pack()
    h3_label = Label(text=h3_var, font=('Helvetica', 19))
    h3_label.pack()
    h4_label = Label(text=h4_var, font=('Helvetica', 15))
    h4_label.pack()
    h5_label = Label(text=h5_var, font=('Helvetica', 13))
    h5_label.pack()
    h6_label = Label(text=h6_var, font=('Helvetica', 11))
    h6_label.pack()
    p_label = Label(text=p_var, font=('Helvetica', 11))
    p_label.pack()
    button_button = Button(text=button_var, command=)
    button_button.pack()